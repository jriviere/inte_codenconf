<?php include 'include/header.php' ?>

<div class="container">
	<?php include 'include/breadcrumb.php' ?>

	<div class="a_propos">
		<h1>Un annuaire des conférences en France</h1>
		<div class="row">
			<div class="col-md-10">
				<p>
					Ce site a été réalisé par un groupe d'étudiants en LP DAWIN, à l'IUT Informatique de Bordeaux, sous le nom de WebAgora. Il s'inscrit dans un projet de développement web. 
				</p>
			</div>
			<div class="col-md-2">
				<img src="assets/img/logowebagora.jpg" alt="Logo WebAgora" id="logo-wa" />
			</div>
		</div>
		<div class="row">
			<p id="developpeurs">Bassam Boulacheb, Rémi Dong, Mathilde Guédon, Maxence Pautrot et Juliette Rivière</p>
			<p>
				Coden' Conf est un mélange entre un moteur de recherche et un réseau social. Il vous permet de vous renseigner sur les conférences du numérique en France à venir. Vous pouvez aussi noter des conférences que vous avez vues ou les revoir en vidéo. En vous connectant, vous pouvez en suivre ou en ajouter.
			</p>
		</div>
		<div id="lexique">
			<p>LP DAWIN : Licence Professionnelle Développeur D'applications Web et Images Numériques</p>
		</div>
	</div>
</div>

<?php include 'include/footer.php' ?>
</html>