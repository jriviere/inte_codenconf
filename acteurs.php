<?php include 'include/header.php' ?>
<div class="container sommaire">
	<?php include 'include/breadcrumb.php' ?>

	<section id="acteurs">
		<h1>Acteurs du projet</h1>
		<article>
			<h2>Organisme a l'origine du projet</h2>
			<p>L'institut universitaire technologique de Bordeaux et à l'origine du projet Coden'Conf</p>
			<img src="assets/img/logo_info_couleur.jpg" alt="Logo IUT Bordeaux">

			<h2>Maître d'oeuvre du projet</h2>
			<p>L'entreprise Web Agora est le maître d'oeuvre du projet Coden'Conf</p>
			<img src="assets/img/logowebagora.jpg" alt="logo web agora">
			<h2>L'Equipe en charge du projet</h2>
			<p>L'Equipe est composée de cinq étudiants:</p>
			<p>Mathilde Guedon : Chef de projet, développeuse back-office</p>
			<p>Bassam Boulacheb : Développeur back-office </p>
			<p>Juliette Riviere : Graphiste, Intégratrice front-end<p>
			<p>Remi Dong : Intégrateur front-end </p>
			<p>Maxence Pautrot : Responsable qualité, Responsable contenu </p>
			
		</article>
	</section>
</div> 
<?php include 'include/footer.php' ?>

</html>