<?php include 'include/header.php' ?>
	<div class="container" id="administration">
		<?php include 'include/breadcrumb.php' ?>

    <h1>Administration</h1>

    <div class="bottom-nav">
      <div class="col-md-12">
        <div class="nav-con">
          <ul>
            <li>
              <i class="fa fa-users"></i>
              <h4><a href="#">Tous les membres</a></h4>
            </li>
            <li>
              <i class="fa fa-comments"></i>
              <h4><a href="#">Toutes les conférences</a></h4>
            </li>
            <li>
              <i class="fa fa-exclamation"></i>
              <h4><a href="#">En attente de validation</a></h4>
            </li>
            <li>
              <i class="fa fa-check-circle"></i>
              <h4><a href="#">Membre(s) en ligne</a></h4>
            </li>
          </ul>
        </div>
      </div>
    </div>    


    <div class="row content">
      <div class="col-md-12">
      	<h2>Derniers membres inscrits : </h2>
        <table class="table table-bordered no-more-tables">
          <thead>
            <tr>
              <th class="text-center" width="5%">Nom</th>
              <th class="text-center" width="12%">Prénom</th>
              <th class="text-center" width="8%">Pseudo</th>
              <th class="text-center" width="7%">Email</th>
              <th class="text-center" width="7%">Nombre de commentaires</th>
              <th class="text-center" width="4%">Date d'inscription </th>
              <th class="text-center" width="4%">Statut </th>
              <th class="text-center" width="4%">Action</th>
            </tr>
          </thead>
          
          <tbody>
            <tr>
              <td class="text-center">Homer</td>
              <td class="text-center">Simpson</td>
              <td class="text-center">Donuts</td>
              <td class="text-center">homer@simpson.fr</td>
              <td class="text-center">42</td>
              <td class="text-center">14/12/2015</td>
              <td class="text-center">membre</td>
              <td class="text-center"><a href="#">Modifier</a> / <a href="#">Supprimer</a></td>

              <tr>
                <td class="text-center">Marge</td>
                <td class="text-center">Simpson</td>
                <td class="text-center">Margy</td>
                <td class="text-center">marge@simpson.fr</td>
                <td class="text-center">28</td>
                <td class="text-center">16/12/2015</td>
                <td class="text-center">membre</td>
                <td class="text-center"><a href="#">Modifier</a> / <a href="#">Supprimer</a></td>

                <tr>
                  <td class="text-center">Lisa</td>
                  <td class="text-center">Simpson</td>
                  <td class="text-center">Lisa</td>
                  <td class="text-center">lisa@simpson.fr</td>
                  <td class="text-center">928</td>
                  <td class="text-center">16/12/2015</td>
                  <td class="text-center">membre</td>
                  <td class="text-center"><a href="#">Modifier</a> / <a href="#">Supprimer</a></td>

                  <tr>
                    <td class="text-center">Bart</td>
                    <td class="text-center">Simpson</td>
                    <td class="text-center">Bart</td>
                    <td class="text-center">bart@simpson.fr</td>
                    <td class="text-center">1</td>
                    <td class="text-center">16/12/2015</td>
                    <td class="text-center">admin</td>
                    <td class="text-center"><a href="#">Modifier</a> / <a href="#">Supprimer</a></td>

                    <tr>
                      <td class="text-center">Maggie</td>
                      <td class="text-center">Simpson</td>
                      <td class="text-center">Maggie</td>
                      <td class="text-center">maggie@simpson.fr</td>
                      <td class="text-center">0</td>
                      <td class="text-center">16/12/2015</td>
                      <td class="text-center">membre</td>
                      <td class="text-center"><a href="#">Modifier</a> / <a href="#">Supprimer</a></td>

                    </tbody>
                  </table>
                  <form id="recherche" action="#" method="post">
                    <label>Rechercher un membre : </label></br><input type="text" name="" placeholder="Nom du membre">
                    <div>
                      <button type="submit" class="btn_search">Rechercher</button>
                    </div>
                  </form>
                </div>
              </div>










              <div class="row content">
                <div class="col-md-12">
                  <h2>Dernières conférences ajoutées : </h2>
                  <table class="table table-bordered no-more-tables">
                    <thead>
                      <tr>
                        <th class="text-center" width="5%">Nom de la conférence</th>
                        <th class="text-center" width="12%">Date de la conférence</th>
                        <th class="text-center" width="8%">Lieu de la conférence</th>
                        <th class="text-center" width="7%">Catégorie de la conférence</th>
                        <th class="text-center" width="7%">Nombre de commentaires</th>
                        <th class="text-center" width="4%">Nombre d'images </th>
                        <th class="text-center" width="4%">Nombre de vidéos </th>
                        <th class="text-center" width="4%">Note moyenne</th>
                        <th class="text-center" width="4%">Action</th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <td class="text-center">conf 1</td>
                        <td class="text-center">18/12/2016</td>
                        <td class="text-center">Bordeaux</td>
                        <td class="text-center">Cybercriminalité</td>
                        <td class="text-center">42</td>
                        <td class="text-center">5</td>
                        <td class="text-center">0</td>
                        <td class="text-center"><a href="#">4,5</a></td>
                        <td class="text-center"><a href="#">Modifier</a> / <a href="#">Supprimer</a></td>

                        <tr>
                          <td class="text-center">conf 2</td>
                          <td class="text-center">12/01/2016</td>
                          <td class="text-center">Paris</td>
                          <td class="text-center">Web</td>
                          <td class="text-center">245</td>
                          <td class="text-center">4</td>
                          <td class="text-center">1</td>
                          <td class="text-center"><a href="#">3,8</a></td>
                          <td class="text-center"><a href="#">Modifier</a> / <a href="#">Supprimer</a></td>

                          <tr>
                            <td class="text-center">conf 3</td>
                            <td class="text-center">22/06/2016</td>
                            <td class="text-center">Marseille</td>
                            <td class="text-center">Algorithme</td>
                            <td class="text-center">22</td>
                            <td class="text-center">5</td>
                            <td class="text-center">0</td>
                            <td class="text-center"><a href="#">4,0</a></td>
                            <td class="text-center"><a href="#">Modifier</a> / <a href="#">Supprimer</a></td>

                            <tr>
                              <td class="text-center">Bordeaux I/O</td>
                              <td class="text-center">16/10/2016</td>
                              <td class="text-center">Bordeaux</td>
                              <td class="text-center">Web/Logiciel</td>
                              <td class="text-center">33</td>
                              <td class="text-center">15</td>
                              <td class="text-center">33</td>
                              <td class="text-center"><a href="#">3,9</a></td>
                              <td class="text-center"><a href="#">Modifier</a> / <a href="#">Supprimer</a></td>

                              <tr>
                                <td class="text-center">conf 5</td>
                                <td class="text-center">17/07/2016</td>
                                <td class="text-center">Lyon</td>
                                <td class="text-center">Réseaux sociaux</td>
                                <td class="text-center">17</td>
                                <td class="text-center">4</td>
                                <td class="text-center">1</td>
                                <td class="text-center"><a href="#">3</a></td>
                                <td class="text-center"><a href="#">Modifier</a> / <a href="#">Supprimer</a></td>

                              </tbody>
                            </table>
                            <form id="recherche" action="#" method="post">
                              <label>Rechercher une conférence : </label></br><input type="text" name="" placeholder="Nom de la conférence">
                              <div>
                                <button type="submit" class="btn_search">Rechercher</button>
                              </div>
                            </form>
                          </div>
                        </div>





                      </div>		
                      <?php include 'include/footer.php' ?>
                    </body>

                    </html>
