<?php include 'include/header.php' ?>
<div class="container" id="agenda">
	<?php include 'include/breadcrumb.php' ?>
	<div class="row">
		<div class"col-xs-12" id="calendrier">
			<h1>Notre agenda </h1>
			<iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=2&amp;bgcolor=%23f5f5f5&amp;src=codenconf%40gmail.com&amp;color=%231B887A&amp;src=fr.french%23holiday%40group.v.calendar.google.com&amp;color=%23125A12&amp;ctz=Europe%2FParis" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
		</div>
		<div id="telechargement_ics">
			<button type="submit" class="btn btn-primary" title="télécharger ics" onclick="indisponible();">Télécharger le .ics</button>
		</div>
	</div>
</div>
<?php include 'include/footer.php' ?>
</html>

