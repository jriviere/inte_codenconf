$( document ).ready(function() {

	// Gestion des onglets de la page une conf

	// Etat de base des onglets au chargement de la page
	$(".rates").hide();
	$(".intervenants").hide();
	$(".informations").hide();

	$("#coms").addClass("active");
	$("#description").addClass("active");

	// Changement de l'état des onglets au clic

	$( "#rates" ).click(function() {
		$(".coms").hide();
		$(".rates").show();
		$("#coms").removeClass("active");
		$("#rates").addClass("active");

	})
	$( "#coms" ).click(function() {
		$(".coms").show();
		$(".rates").hide();
		$("#rates").removeClass("active");
		$("#coms").addClass("active");

	})

	$( "#description" ).click(function() {
		$(".informations").hide();
		$(".description").show();
		$("#description").addClass("active");
		$("#information").removeClass("active");
	})
	
	$( "#informations" ).click(function() {
		$(".description").hide();
		$(".informations").show();
		$("#description").removeClass("active");
		$("#information").addClass("active");
	})

	// Remonter en haut de la page

	$('.arrow_up').click(function(event) {
		event.preventDefault();	
		$('html, body').animate({scrollTop: 0}, 300);
	})

	// dropdown du menu qui se ferme au clique exterieur au canvas

	var targetBouton = $('button.navbar-toggle');
	var targetMenu = $('.navbar-collapse');	

	$('html').bind("click touchstart", function(event) {
		if(!$(event.target).closest(targetMenu).length && $('.collapse.in').length) {
			if(!$(event.target).closest(targetBouton).length) {
				targetBouton.trigger("click");
			};
		};
	});


});

(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.5&appId=974267379279143";
		fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function suppression_compte(){
	var texte = "Cette action sera définitive. Etes vous sûr de vouloir supprimer votre compte ?";
	return confirm(texte);
}

function indisponible(){
	var texte = "Cette action est indisponible pour le moment.";
	return alert(texte);
}

function initMap() {
  var eais = {lat: 50.3755849, lng: 3.0655826};
  var web2day = {lat: 47.2382006, lng: -1.6311773};
  var bim = {lat: 48.892386, lng: 2.234444};

  // Create a map object and specify the DOM element for display.
  var map = new google.maps.Map(document.getElementById('map'), {
    center: bim,
    scrollwheel: false,
    zoom: 6
  });

  // Create a marker and set its position.
  var marker = new google.maps.Marker({
    map: map,
    position: eais,
    title: 'EAIS'
  });

  var marker2 = new google.maps.Marker({
    map: map,
    position: web2day,
    title: 'Web2Day'
  });

  var marker3 = new google.maps.Marker({
    map: map,
    position: bim,
    title: 'BIM'
  });

}