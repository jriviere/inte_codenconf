<?php include 'include/header.php' ?>

<div class="container sommaire" id="saisie">
	<?php include 'include/breadcrumb.php' ?>

	<div class="saisie_conf">

		<h1>Ajouter une conférence</h1>
		<div class="title-bar"></div>

		<form id="ajout_conf" action="indisponible.php" method="post">
			<h2>Nom de la conférence : </h2> <input type="text" id="nom_conference" placeholder="Nom de la conférence" title="Nom"/> </br>
			<h2>Lieu de la conférence :</h2> <input type="text" id="lieu_conference" placeholder="Ville"  title="Ville"/></br>
			<h2>Date de début (jj/mm/aaaa) : </h2><input type="date"/><h2>Date de fin (jj/mm/aaaa) : </h2> <input type="date"/></br>					
			<h2>Description : </h2><textarea placeholder="Description de la conférence" id="description_conférence"  title="Description"></textarea> </br>
			<h2>Langue de la conférence : </h2> 
			<div id="selection_lang">
				<select name="langue" id="langue">
					<option value="francais">Français</option>
					<option value="anglais">Anglais</option>
					<option value="autre">Autre</option>
				</select>
			</div>

			<div id="checkbox">
				<h2>Les catégories de votre conférences : </h2>
				<input type="checkbox" name="réseaux_sociaux" id="reseaux_sociaux" /> <label for="résaux_sociaux">Réseaux Sociaux</label>
				<input type="checkbox" name="web" id="web" /> <label for="web">Web</label>
				<input type="checkbox" name="cybercriminalité" id="cybercriminamité" /> <label for="cybercrimnalité">Cybercriminalité</label><br />
				<input type="checkbox" name="logiciel" id="logiciel" /> <label for="logiciel">Logiciel</label>
				<input type="checkbox" name="algorithme" id="algorithme "/> <label for="alorithme">Algorithme</label>
				<input type="checkbox" name="autre" id="autre "/> <label for="autre">Autres</label>
			</div>

			<div id="confirmation_saisie">
				<button type="submit" class="btn btn-primary">Ajouter la conférence</button>
			</div>
		</form>
	</div>

</div>
<?php include 'include/footer.php' ?>


</html>