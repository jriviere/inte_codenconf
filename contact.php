<?php include 'include/header.php' ?>

	<div class="container">
		<?php include 'include/breadcrumb.php' ?>
		
		<div class="formulaire_contact">

			<h1>Nous contacter</h1>
			<div class="title-bar"></div>

			<form id="contact" action="indisponible.php" method="post">
				<div class="left">
					<input type="text" id="nom_contact" placeholder="Nom" required="required" title="Nom"/>
					<input type="email" id="email_contact" placeholder="Email" required="required" title="Email"/>
					<input type="text" id="sujet_contact" placeholder="Sujet" required="required" title ="Sujet du message"/>
				</div>
				<div class="right">
					<textarea placeholder="Message" id="message_contact" required="required" title="message"></textarea>
				</div>
				<div>
					<button type="submit" class="btn btn-primary">Envoyer</button>
				</div>
			</form>
		</div>
	</div>
	 
	<?php include 'include/footer.php' ?>
</html>