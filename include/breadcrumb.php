<?php 
	//Récupération url sans localhost (en prod : sans codenconf.fr)
	$url = $_SERVER['PHP_SELF'];

	//Récupération de chaque partie de l'url dans tableau
	// Ne pas prendre $array_url[0] car vide
	$array_url = explode("/", $url);
	unset($array_url[1]);
	$array_url = array_filter($array_url);

	$conferences = array("une conf bim", "une conf web2day", "top Conferences", "a proximite", "conferences saisie");

    $pages_utilisateur = array("mon compte", "creer_conference", "mes_conferences", "mes_abonnements", "profil");

?>

<div class="row breadcrumb">	
	<div class="col-xs-12 no_padding">	
		<ul class="no_padding">	
			<li><a class="active" href="index.php" title= "accueil"><i class="fa fa-home fa-2x"></i></a></li>
			<li><a href="index.php">Accueil</a></li>

			<?php 
				foreach($array_url as $page) {
					if($page != "index.php" && substr($page, -4) == ".php")  {
						//Enlève le .php
						$rest = substr($page, 0, -4);
						//Remplace les "_" par des espaces
						$rest = str_replace("_", " ", $rest);

						if (in_array($rest, $conferences))
							echo "<li><a href='search.php' title='conférences'>conférences</a></li>";
						if (in_array($rest, $pages_utilisateur))
							echo "<li><a href='profil.php' title='utilisateur'>utilisateur</a></li>";

						if($page == "conferences_saisie.php"){
							echo "<li><a href='". $page . "' title='creer une conference'>creer une conference</a></li>";
						}
						else
							echo "<li><a href='". $page . "' title='" . $rest . "'>" . $rest . "</a></li>";
					}
				}
			?>
		</ul>
	</div>
</div>