<!-- Template de card -->
<div class="col-xs-12 ">
	<div class="card">
		<div class="card_titre">
			<div class="card_author">
				<div class="media">
					<a class="pull-left" href="#">
						<img class="media-object dp img-circle" src="http://lorempixel.com/400/400/technics" alt="Image Utilisateur">
					</a>									
				</div>	
				<div class="card_author_content">
					<span class="card_title">
						<a href="une_conf_ieee.php" title="conférence IEEE">IEEE 2015 EAIS</a>
					</span>
					<span class="card_sub_title">Du 1er au 3 décembre 2015</span>
				</div>
			</div>
		</div>
		<div class="card_image">
			<a href="une_conf_ieee.php" title="conféence IEEE">
				<img class="img-responsive" src="assets/img/ieee_logo.png" alt="Logo IEEE">
			</a>
		</div>

		<div class="card_content">
			<a href="une_conf_ieee.php" title="Conférence IEEE">
				<p class="sous_titre">
					Par IEEE / 1,2,3 décembre 2015 / Inovation / Progrès
				</p>
				<p>Discussions autour du domaine des systèmes intelligents évolutif... 
				</p>
			</a>		
		</div>

		<div class="card_action">
			<div class="no_padding col-xs-8 rating">
				<span class="glyphicon glyphicon-star-empty"></span>
				<span class="glyphicon glyphicon-star-empty"></span>
				<span class="glyphicon glyphicon-star-empty"></span>
				<span class="glyphicon glyphicon-star-empty"></span>
				<span class="glyphicon glyphicon-star-empty"></span>
				<span class="nb_votants" title="nombre de votants">(0)</span>
			</div>
			<div class="no_padding col-xs-4 actions_right">
				<span class="glyphicon glyphicon-heart-empty"></span>

				<span  class="glyphicon glyphicon-share-alt" title="Partager" data-toggle="modal" data-target="#myModal3">

				</span>
				<a href="une_conf_ieee.php" title="Conférence IEEE"><i class="glyphicon glyphicon-plus"></i></a>
			</div>
		</div>
	</div>
</div>





<!-- Modal share -->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span>
					<span class="sr-only">Fermer</span>
				</button>
				<h4 class="modal-title myModalLabel">
					<i class="glyphicon glyphicon-share-alt"></i>
					Partager
				</h4>
			</div>
			<div class="modal-body">
				<p>
					<a title="Facebook" href="http://www.facebook.com/share.php?u=http://codenconf.fr/inte/une_conf_ieee.php">
						<span class="fa-stack fa-lg">
							<i class="fa fa-square-o fa-stack-2x"></i>
							<i class="fa fa-facebook fa-stack-1x"></i>
						</span>
					</a>
					<a title="Twitter"  href="http://www.twitter.com/share?url=http://codenconf.fr/inte/une_conf_ieee.php" class="twitter-share-button" data-via="CodenConf" data-hashtags="ConféFrance">
						<span class="fa-stack fa-lg">
							<i class="fa fa-square-o fa-stack-2x"></i>
							<i class="fa fa-twitter fa-stack-1x"></i>
						</span>
					</a>
					<a title="Google+" href="https://plus.google.com/share?url=http://codenconf.fr/inte/une_conf_ieee.php">
						<span class="fa-stack fa-lg">
							<i class="fa fa-square-o fa-stack-2x"></i>
							<i class="fa fa-google-plus fa-stack-1x"></i>
						</span>
					</a>

					<!-- Voir si implementation future ou non --> 
					
					<!-- <a title="Linkedin" href="">
						<span class="fa-stack fa-lg">
							<i class="fa fa-square-o fa-stack-2x"></i>
							<i class="fa fa-linkedin fa-stack-1x"></i>
						</span>
					</a>
					<a title="E-mail" href="">
						<span class="fa-stack fa-lg">
							<i class="fa fa-square-o fa-stack-2x"></i>
							<i class="fa fa-envelope fa-stack-1x"></i>
						</span>
					</a>
					<a title="Print" href="">
						<span class="fa-stack fa-lg">
							<i class="fa fa-square-o fa-stack-2x"></i>
							<i class="fa fa-print fa-stack-1x"></i>
						</span>
					</a> -->
				</p>

				<h2>
					<i class="fa fa-envelope"></i>
					Newsletter
				</h2>

				<p>Souscrivez à notre Newsletter pour ne manquer aucune conférence !</p>

				<form action="#" method="post">
					<div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-envelope"></i>
						</span>
						<input type="email" class="form-control" placeholder="votre@email.com">
					</div>
					<br />
					<button type="submit" value="sub" name="sub" class="btn btn-primary">
						<i class="fa fa-share"></i>
						S'inscrire maintenant !
					</button>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
			</div>
		</div>
	</div>
</div>
