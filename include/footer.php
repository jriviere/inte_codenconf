<div class="row  no_margin">
	<div class="col-xs-12 arrow_container">	
		<a class="arrow_up" href="#top" title="retourner en haut"><i class=" fa fa-arrow-circle-o-up"></i></a>
	</div>
</div>

<footer>
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-8 col-sm-3 col-xs-push-4 col-sm-push-0 logo_sociaux">
					<a href="https://www.facebook.com/codenconf/?fref=ts" class="fa fa-facebook fa-2x" title="Facebook" target="_blank"></a>
					<a href="https://twitter.com/CodenConf" class="fa fa-twitter fa-2x"  title="Twitter" target="_blank"></a>
					<a href="http://www.iut.u-bordeaux1.fr/info/" title="IUT de Bordeaux" target="_blank"><img class="img_iut" src="assets/img/logo_info_couleur.jpg" alt="logo IUT Info"/></a>
				</div>
				<div class="col-xs-4 col-sm-7 col-xs-pull-8 col-sm-pull-0 ">
					<ul class="menu_footer">
						<li><a href="acteurs.php" title="Acteurs">Acteurs</a></li>

						<li><a href="a_propos.php" title="A propos">À propos</a></li>

						<li><a href="newsletter.php" title="Newsletter">Newsletter</a></li>
						<li><a href="mentions_legales.php" title="Mentions légales">Mentions légales</a></li>
						<li><a href="plan_du_site.php" title="Plan du site">Plan du site</a></li>
						<li><a href="contact.php" title="contact">Contact</a></li>
					</ul>
				</div>
				<a class="hidden-xs col-xs-2" rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/fr/"  title="licence">
					<img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/3.0/fr/88x31.png" />
				</a>
			</div>
		</div>
	</div>
</footer>

<script src="assets/js/jquery-2.1.4.min.js"></script>	
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDc2po7vjc31RkfKdPulCIeGWjVVuzEhkA&callback=initMap">
</script>

</body>
