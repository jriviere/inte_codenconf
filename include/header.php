<!DOCTYPE HTML>
<html lang="fr">
<head>
	<title>Coden' Conf</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Suivez les conférences du numérique partout en France et partagez celles qui vous intéressent." />
	<link rel="shortcut icon" type="image/png" href="assets/img/favicon.png" />
	<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/main.css">
	<link rel="stylesheet" href="assets/css/print.css" media="print">
	<link rel="stylesheet" href="assets/fonts/fontawesome/css/font-awesome.min.css">
	<!-- Google+ Share -->
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<?php 
		$url = $_SERVER['PHP_SELF'];
		$url_full = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
	?>
	<link rel="canonical" href="<?php echo $url;?>"/>
	<!-- Facebook Share -->
	<div id="fb-root"></div>
</head>
<body>
<?php include 'include/analytics.php' ?>
	<header>
		<?php include 'include/menu.php' ?>
		<div class="content content_header">
			<div class="container">
				<div class="row">
					<form class="navbar-form navbar">
						<div class="form-group">
							<input type="text" class="recherche_principale form-control" id="barre_recherche" placeholder="Rechercher une conférence" title="rechercher une conférence">
						</div>
						<button type="submit" class="btn btn-default btn-search" formaction="search.php">Rechercher</button>		
					</form>
				</div>
				<div class="row">
					<button class="btn btn-default more_filters">Plus de filtres</button>	
				</div>
			</div>
		</div>
	</header>

