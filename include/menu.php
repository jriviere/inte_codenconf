<nav class="navbar navbar-inverse navbar-fixed-top navbar-absolute">
	<div class="container"> <!-- originalement container-fluid -->
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php" title="accueil">
				<img src="assets/img/logo.png" alt="logo coden' conf">
			</a>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

			<ul class="nav navbar-nav navbar-right">
				<li><a href="search.php?top=1" title="top conférences">Top Conférences</a></li>
				<li><a href="search.php?ville=bordeaux" title="à proximité">À proximité</a></li>
				<li><a href="agenda.php" title="agenda">Agenda</a></li>

				<li >
					<div class="badge_user">
						<div class="media">
							<a class="pull-right" href="profil.php" title="menu utilisateur">
								<img class="media-object dp img-circle" src="http://lorempixel.com/400/400/cats" alt="image profil">
							</a>									
						</div>
					</div>	
					</li>
					<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="menu utilisateur">
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="inscription.php" title="s'inscrire">S'inscrire</a></li>
						<li><a href="login.php" title="se connecter">Se connecter</a></li>
						<li><a href="administration.php" title ="administration">Administration</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="mon_compte.php" title="mon compte">Mon compte</a></li>
						<li> <a id="important" href="conferences_saisie.php" title="créer une conférence">Créer une conférence</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>