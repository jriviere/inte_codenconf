<?php include 'include/header.php' ?>
	<div class="container sommaire" id="index">
		<?php include 'include/breadcrumb.php' ?>

		<h1>Accueil</h1>

		<div class="row titre_type_card">
			<div class="col-xs-6">Conférence la mieux notée :</div>
			<div class="col-xs-6 hidden-xs">Conférence proche de chez vous :</div>
		</div>

		<!-- 	Appel des cards -->
		<div class="row">

			<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-1">
				<?php include 'include/card1.php' ?>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
				<div class="visible-xs titre_type_card">Conférence proche de chez vous :</div>

				<?php include 'include/card2.php' ?>
			</div>
		</div>

		<div class="row titre_type_card">
			<div class="col-xs-6">Conférence la plus populaire :</div>
			<div class="col-xs-6 hidden-xs">Prochaine conférence :</div>
		</div>

		<!-- 	Appel des cards -->
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-1">
				<?php include 'include/card1.php' ?>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
				<div class="visible-xs titre_type_card">Prochaine conférence :</div>

				<?php include 'include/card3.php' ?>
			</div>
		</div>

	</div> 

	<?php include 'include/footer.php' ?>
</html>