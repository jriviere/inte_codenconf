<?php include 'include/header.php' ?>
	<div class="container sommaire" id="inscription">
		<?php include 'include/breadcrumb.php' ?>
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
				<form role="form">
					<h1>Inscription</h1>
					<hr class="colorgraph">
					<div class="form-group">
						<input type="text" name="pseudo" id="pseudo" class="form-control input-lg" placeholder="Pseudo" tabindex="4" title="pseudo">
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<input type="text" name="first_name" id="first_name" class="form-control input-lg" placeholder="Prénom" tabindex="1" title="prénom">
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<input type="text" name="last_name" id="last_name" class="form-control input-lg" placeholder="Nom de famille" tabindex="2" title="nom">
							</div>
						</div>
					</div>
					<div class="form-group">
						<input type="email" name="email" id="email" class="form-control input-lg" placeholder="Adresse email" tabindex="4" title="email">
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Mot de passe" tabindex="5" title="mot de passe">
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Confirmez le mot de passe" tabindex="6" title="confirmation du mot de passe">
							</div>
						</div>
					</div>
					<hr class="colorgraph">
					<div class="form-group">
						<input type="submit" value="S'inscrire" class="btn btn-primary btn-block btn-lg" tabindex="7" title="s'inscrire">
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php include 'include/footer.php' ?>
</html>