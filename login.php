<?php include 'include/header.php' ?>
	<div class="container" id="login">
		<?php include 'include/breadcrumb.php' ?>
		<div class="row">
			<div class="col-xs-10 col-sm-4 col-md-4 col-xs-offset-1 col-sm-offset-4 col-md-offset-4">
				<form role="form">
					<h1>Se connecter</h1>
					<hr class="colorgraph">
					<div class="form-group">
						<input type="text" name="pseudo" id="pseudo" class="form-control input-lg" placeholder="Pseudo" tabindex="4" title="pseudo">
					</div>
					<div class="form-group">
						<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Mot de passe" tabindex="5" title="mot de passe">
					</div>
					<hr class="colorgraph">
					<div class="form-group">
						<input type="submit" value="Se connecter" class="btn btn-primary btn-block btn-lg" tabindex="7" title="se connecter">
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php include 'include/footer.php' ?>
</html>