<?php include 'include/header.php' ?>
<div class="container" id="mon_compte">
	<?php include 'include/breadcrumb.php' ?>

	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<form role="form">
				<h1>Mes informations personnelles</h1>

				<hr class="colorgraph">
				<div class="form-group">
					<h2><span class="label label-info">Pseudo</span></h2>
					<input type="text" name="pseudo" id="pseudo" class="form-control input-lg" placeholder="Pseudo" tabindex="4" title="pseudo" value="El catouccino">
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">
							<h2><span class="label label-info">Nom</span></h2>
							<input type="text" name="first_name" id="first_name" class="form-control input-lg" placeholder="Prénom" tabindex="1" title="prénom">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">
							<h2><span class="label label-info">Prénom</span></h2>
							<input type="text" name="last_name" id="last_name" class="form-control input-lg" placeholder="Nom de famille" tabindex="2" title="nom">
						</div>
					</div>
				</div>
				<div class="form-group">
					<h2><span class="label label-info">Email</span></h2>
					<input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email" tabindex="4" title="email" value="el_catouccino@gmail.com">
				</div>

				<hr class="colorgraph">
				<div class="form-group">
					<input type="submit" value="Sauvegarder les modifications" class="btn btn-primary btn-block btn-lg" tabindex="7" title="sauvegarder les modifications" onclick="sauvegarder_compte();">
				</div>

				<hr class="colorgraph">
				<div class="form-group" id="suppression">
					<input type="submit" value="Supprimer mon compte" class="btn btn-danger btn-block btn-md" tabindex="7" title="Supprimer mon compte" onClick="suppression_compte();">
				</div>
			</form>
		</div>
	</div>
</div> 

<?php include 'include/footer.php' ?>
</html>