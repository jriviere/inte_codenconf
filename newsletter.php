<?php include 'include/header.php' ?>
<div class="container sommaire" id="newsletter">
	<?php include 'include/breadcrumb.php' ?>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
			<form role="form">
				<h1>Newsletter</h1>
				<hr class="colorgraph">
				<div class="form-group">
					<input type="email" name="email" id="email" class="form-control input-lg" placeholder="Adresse email" tabindex="4" title="email">
				</div>
				<hr class="colorgraph">
				<div class="form-group">
					<input type="submit" value="Recevoir la newsletter" class="btn btn-primary btn-block btn-lg" tabindex="7" title="se connecter" onClick="indisponible();">
				</div>
			</form>
		</div>
	</div>
</div>
<?php include 'include/footer.php' ?>
</html>