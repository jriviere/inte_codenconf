<?php include 'include/header.php' ?>
	<div class="container sommaire" id="plan_site">
		<?php include 'include/breadcrumb.php' ?>

		<section>
			<h1>Plan du site</h1>
			<article>
				<ul>
					<li><h2><a href="index.php" title="Accueil">Accueil</a></h2>
						<ul>
							<li><h3><a href="search.php" title="Conférences">Conférences</a></h3>
								<ul>
									<li><a href="search.php?top=1" title="Accueil">Top Conférences</a></li>
									<li><a href="search.php?ville=bordeaux" title="A proximité">À proximité</a></li>
									<li><a href="une_conf_bim.php" title="Détail conférence BIM">Conférence BIM</a></li>
									<li><a href="une_conf_web2day.php" title="Détail conférence Web2Day">Conférence Web2Day</a></li>
									<li><a href="une_conf_ieee.php" title="Détail conférence IEEE">Conférence IEEE</a></li>
								</ul>
							</li>
							<li><h3><a href="acteurs.php" title="Acteurs">Acteurs</a></h3></li>
							<li><h3><a href="a_propos.php" title="À propos">À propos</a></h3></li>
							<li><h3><a href="newsletter.php" title="Newsletter">Newsletter</a></h3></li>
							<li><h3><a href="mentions_legales.php" title="Mentions légales">Mentions légales</a></h3></li>
							<li><h3><a href="plan_du_site.php" title="Plan du site">Plan du site</a></h3></li>
							<li><h3><a href="contact.php" title="Contact">Contact</a></h3></li>
							<li><h3><a href="agenda.php" title="Agenda">Agenda</a></h3></li>
							<li><h3><a href="inscription.php" title="S'inscrire">S'inscrire</a></h3></li>
							<li><h3><a href="login.php" title="Se connecter">Se connecter</a></h3>
							<li><h3><a href="administration.php" title="Administration">Administration</a></h3>
							<li><h3><a href="profil.php" title="Profil utilisateur">Profil utilisateur</a></h3>
								<ul>
									<li><a href="mon_compte.php" title="Mon compte">Mon compte</a></li>
									<li><a href="creer_conference.php" title="Créer une conférence">Ajouter une conférence</a></li>
								</ul>
							</li>
						</ul>
					</li>
				</ul>
			</article>
		</section>
	</div> 

	<?php include 'include/footer.php' ?>

</html>
