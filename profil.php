<?php include 'include/header.php' ?>
	<div class="container">
		<?php include 'include/breadcrumb.php' ?>

	<div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">
		<div class="profil">
					<h1>Profil</h1>
					<hr class="colorgraph">
						<div class="profil_name">
							<div class="profil_avatar">
								<a class="pull-left" href="#">
								<img class="media-object dp img-circle" src="http://lorempixel.com/400/400/cats" alt="Image Utilisateur">
								</a>									
							</div>	
							<div class="profil_pseudo">
								<span>El catouccino</span>	
							</div>
						</div>

						<div class="profil_desc">
							<div><span class="labl">Type d'utilisateur : </span><span>Membre</span></div>
							<div><span class="labl">Inscrit depuis le : </span><span>05/11/2015</span></div>
							<div><span class="labl">Nombre de conférences ajoutées : </span><span>1</span></div>	
							<div><span class="labl">Nombre de commentaires : </span><span>14</span></div>
						</div>

						<div class="profil_conf">
							<ul class="section_hidden">
								  <li class="section_hidden_item">
								  	<input id="check_box_conf" class="checkbox" type="checkbox"/>
								    <label for="check_box_conf">Conferences ajoutées</label>
								<div class="content ">
										<div class="col-sm-12 col-md-6 no_padding">
										<?php include 'include/card2.php' ?>
										</div>
								    </div>
								  </li>
							</ul>
						</div>

						<div class="profil_favs">
							<ul class="section_hidden">
								  <li class="section_hidden_item">
								  	<input id="check_box" class="checkbox" type="checkbox"/>
								    <label for="check_box">Favoris</label>
								<div class="content ">
								  		<div class="col-sm-12 col-md-6 no_padding" >
										<?php include 'include/card1.php' ?>
										</div>
										<div class="col-sm-12 col-md-6 no_padding">
										<?php include 'include/card2.php' ?>
										</div>
										<div class="col-sm-12 col-md-6 no_padding">
										<?php include 'include/card3.php' ?>
										</div>
								    </div>
								  </li>
							</ul>
						</div>


		</div>



					<hr class="colorgraph">
			</div>
		</div>
 
	<?php include 'include/footer.php' ?>


</html>