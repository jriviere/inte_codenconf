<?php include 'include/header.php' ?>
<div class="container" id="search">
	<?php include 'include/breadcrumb.php' ?>

	<div class="row col-xs-12">
		<h1 class="pull-left">Résultats pour : mot-clé</h1>
		<?php include 'include/pagination.php' ?>
	</div>	
	<div class="row search_res">

		<div class="col-xs-12 col-sm-6 col-md-4">
			<?php include 'include/card3.php' ?>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4">
			<?php include 'include/card2.php' ?>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4">
			<?php include 'include/card1.php' ?>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4">
			<?php include 'include/card2.php' ?>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4">
			<?php include 'include/card1.php' ?>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4">
			<?php include 'include/card3.php' ?>
		</div>


	</div>

	<div class="col-xs-12">
		<?php include 'include/pagination.php' ?>
	</div>

	<div id="map"></div>

</div>



<?php include 'include/footer.php' ?>

