<?php include 'include/header.php' ?>
<div class="container une_conf">
	<?php include 'include/breadcrumb.php' ?>

	<div class="row titre_conf hidden-xs">             
		<h1>BIM World 2016</h1>
		<hr/>     
		<h2>Paris  le 07/04/2016 de 09H00 à 18H00</h2>
	</div>

	<div class="row desc_conf">
		<div class="col-xs-6 col-sm-4 img_conf">
			<img class="img-responsive" src="assets/img/bim_une_conf_mini.jpg" alt="">
			<div class="no_padding hidden-xs rating">
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star-empty	"></span>
				<span>(52)</span>
				<span class="glyphicon glyphicon-heart"></span>
			</div>
		</div>
		<div class="col-xs-6 visible-xs">
			<div class="col-xs-12 titre_conf">             
				<h1>BIM World 2016</h1>
				<hr/>     
				<h2>Paris  le 07/04/2016 de 09H00 à 18H00</h2>
			</div>
			<div class="col-xs-12 no_padding rating">
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star-empty	"></span>
				<span>(52)</span>
				<span class="glyphicon glyphicon-heart"></span>
			</div>
			<div class="col-xs-12 logo_sociaux">
				<a href="http://www.facebook.com/share.php?u=<?php echo $url_full; ?>" class="fa fa-facebook fa-2x" title="Facebook"></a>
				<a href="https://twitter.com/share" class="fa fa-twitter fa-2x"  data-via="CodenConf" data-hashtags="ConféFrance" title="Twitter"></a>
				<a href="https://plus.google.com/share?url=<?php echo $url_full; ?>" title="google plus" ><img class="img_google_plus" src="assets/img/button-gplus.png" alt="logo partage google plus"/></a>
			</div>
		</div>

		<div class="col-xs-12 col-sm-8 container_desc">
			<div class="row no_margin onglets">
				<ul>
					<li class="col-xs-4 col-sm-3 active" id="description">Description</li>
					<li class="col-xs-4 col-sm-3" id="informations">Informations</li>
					<li class="col-xs-2 hidden-xs hidden-sm">
						<div class="fb-share-button" data-href="<?php echo $url;?>" data-layout="button_count"></div>
					</li>
					<li class="col-xs-2 hidden-xs hidden-sm">
						<a href="https://twitter.com/share" class="twitter-share-button" data-via="CodenConf" data-hashtags="ConféFrance"></a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
					</li>
					<li class="col-xs-2 hidden-xs hidden-sm"><g:plus action="share"  annotation="bubble"></g:plus></li>
				</ul>
			</div>
			<div class="row no_margin contenu_desc description">
				<p>- Un cycle de conférences international pour présenter les meilleures pratiques du numérique pour la construction à travers le monde et avoir les bons repères sur le marché français,</p>
				<p>- Des Master Class pour approfondir les pratiques professionnelles et découvrir les innovations,</p>
				<p>- Un forum des métiers et compétences du numérique pour la construction,</p>
				<p>- Un espace démonstration unique pour vivre l’expérience de la 3D.</p>
			</div>
			<div class="row no_margin contenu_desc informations">
				<p><b>	Adresse : </b></p>
				<p> Espace Grande Arche - Paris La Défense</p>
				<p><b> Exhibition</b> (Accès nominatif pour les 2 jours à l’exposition) : </p>
				<p> 50€</p>
				<p><b> Conférences </b>(Accès nominatif pour les 2 jours à l’exposition et aux conférences) : </p>
				<p> 300€</p>
				<p><b> VIP </b>Accès nominatif pour les 2 jours à l’exposition, aux conférences et au VIP Lounge (rafraichissements et collations, rencontres avec les intervenants et VIP, accès Wifi,...) : </p>
				<p> 500€</p>
			</div>
		</div>
	</div>

	<div class="row rates_coms">
		<div class="col-xs-12">
			<div class="row no_margin onglets">
				<ul>
					<li class="col-xs-4 col-sm-3 col-md-2" id="coms">Commentaires</li>
					<li class="col-xs-4 col-sm-3 col-md-2" id="rates">Détail des notes</li>
				</ul>
			</div>
			<div class="row no_margin coms">
				<div class="col-xs-12 container_coms">
					<div class="row left_chat">							
						<div class="col-xs-3 col-sm-2 badge_user">
							<div class="media">
								<a class="pull-right" href="#">
									<img class="media-object dp img-circle" src="http://lorempixel.com/400/400/people">
									<span>Alex</span>
								</a>									
							</div>
						</div>	
						<div class="col-xs-9 col-sm-10">
							<div class="bubble bubble-border">
								<p class="date_coms">09/02/2015 à 12:30</p>
								<hr class="separateur"/>
								<p>
									Bonjour, petite information complémentaire : la conférence se déroule à  l’Espace Grande Arche - Paris La Défense.
									Avez-vous le programme ?
								</p>
							</div>
						</div>						
					</div>
					
					<div class="row right_chat">
						<div class="col-xs-3 col-sm-2 badge_user">
							<div class="media">
								<a class="pull-left" href="profil.php">
									<img class="media-object dp img-circle" src="http://lorempixel.com/400/400/cats">
									<span>El Catouccino</span>
								</a>									
							</div>
						</div>
						<div class="col-xs-9 col-sm-10">
							<div class="bubble bubble-border">
								<p class="date_coms">09/02/2015 à 12:30</p>
								<hr class="separateur"/>
								<p>
									Vous pouvez retrouver tout le programme en ligne à cette adresse : http://bim-w.com/program?lang=fr

								</p>	
							</div>
							

						</div>
					</div>

					<div class="row left_chat">							
						<div class="col-xs-3 col-sm-2 badge_user">
							<div class="media">
								<a class="pull-right" href="#">
									<img class="media-object dp img-circle" src="http://lorempixel.com/400/400/people">
									<span>Alex</span>
								</a>									
							</div>
						</div>	
						<div class="col-xs-9 col-sm-10">
							<div class="bubble bubble-border">
								<p class="date_coms">09/02/2015 à 12:30</p>
								<hr class="separateur"/>
								<p>Merci beaucoup de votre réponse !</p>	
							</div>
						</div>						
					</div>
					<div class="commenter">
						<form action="indisponible.php" method="post">
							<textarea placeholder="Ajouter un commentaire" class="col-xs-12 form-control" title="nouveau_commentaire" required="required"></textarea>
							<button type="submit" class="btn btn-primary" title="commenter">Commenter</button>
						</form>
					</div>
				</div>

			</div>
			<div class="row no_margin rates">
				<div class="col-xs-12 container_rates">
					<?php include "include/rates_system.php" ?>
				</div>

			</div>
		</div>

	</div>
</div> 
<?php include 'include/footer.php' ?>

</html>
