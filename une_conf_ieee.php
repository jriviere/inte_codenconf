<?php include 'include/header.php' ?>
<div class="container une_conf">
	<?php include 'include/breadcrumb.php' ?>

	<div class="row titre_conf hidden-xs">             
		<h1>IEEE 2015 EAIS</h1>
		<hr/>     
		<h2>Douais du 1er au 3 décembre 2015</h2>
	</div>

	<div class="row desc_conf">
		<div class="col-xs-6 col-sm-4 img_conf">
			<img class="img-responsive" src="assets/img/ieee_grand.jpg" alt="Logo IEEE">
			<div class="no_padding hidden-xs rating">
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star-empty	"></span>
				<span>(42)</span>
				<span class="glyphicon glyphicon-heart"></span>
			</div>
		</div>
		<div class="col-xs-6 visible-xs">
			<div class="col-xs-12 titre_conf">             
				<h1>IEEE 2015 EAIS</h1>
				<hr/>     
				<h2>Douais du 1er au 3 décembre 2015</h2>
			</div>
			<div class="col-xs-12 no_padding rating">
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star-empty	"></span>
				<span>(42)</span>
				<span class="glyphicon glyphicon-heart"></span>
			</div>
			<div class="col-xs-12 logo_sociaux">
				<a href="http://www.facebook.com/share.php?u=<?php echo $url_full; ?>" class="fa fa-facebook fa-2x" title="Facebook"></a>
				<a href="https://twitter.com/share" class="fa fa-twitter fa-2x"  data-via="CodenConf" data-hashtags="ConféFrance" title="Twitter" ></a>
				<a href="https://plus.google.com/share?url=<?php echo $url_full; ?>" title="IUT de Bordeaux" ><img class="img_iut" src="assets/img/button-gplus.png" alt="logo partage google plus"/></a>
			</div>
		</div>



		<div class="col-xs-12 col-sm-8 container_desc">
			<div class="row no_margin onglets">
				<ul>
					<li class="col-xs-4 col-sm-3 active" id="description">Description</li>
					<li class="col-xs-4 col-sm-3" id="informations">Informations</li>
					<li class="col-xs-2 hidden-xs hidden-sm">
						<div class="fb-share-button" data-href="<?php echo $url;?>" data-layout="button_count"></div>
					</li>
					<li class="col-xs-2 hidden-xs hidden-sm">
						<a href="https://twitter.com/share" class="twitter-share-button" data-via="CodenConf" data-hashtags="ConféFrance">Tweet</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
					</li>
					<li class="col-xs-2 hidden-xs hidden-sm"><g:plus action="share"  annotation="bubble"></g:plus></li>
				</ul>
			</div>
			<div class="row no_margin contenu_desc description">
				<p>IEEE 2015 EAIS fourni une atmosphère amicale et sera un forum international de premier plan
					en mettant l'accent sur la discussion des progrès récents, l'échange d'innovations récentes, et le
					contour des importants défis à venir dans le domaine des systèmes intelligents évolutif
					et d'adaptatif.
				</p>
			</div>
			<div class="row no_margin contenu_desc informations">
				<p><b>	Adresse : </b></p>
				<p>Résidence Descartes, Ecole Nationale Supérieure des Mines de Douai
					263, rue du Grand Bail
					Douai-France</p>
					<p><b> Site de l'Evenement : </b></p>
					<a href="http://conference.mines-douai.fr/eais2015/informations/" target="_blank" title="informations EAIS">Informations EAIS</a>
				</div>
			</div>
		</div>

		<div class="row rates_coms">
			<div class="col-xs-12">
				<div class="row no_margin onglets">
					<ul>
						<li class="col-xs-4 col-sm-3 col-md-2" id="coms">Commentaires</li>
						<li class="col-xs-4 col-sm-3 col-md-2" id="rates">Détail des notes</li>
					</ul>
				</div>
				<div class="row no_margin coms">
					<div class="col-xs-12 container_coms">
						<div class="row left_chat">							
							<div class="col-xs-3 col-sm-2 badge_user">
								<div class="media">
									<a class="pull-right" href="#">
										<img class="media-object dp img-circle" src="http://lorempixel.com/400/400/people">
										<span>Alex</span>
									</a>									
								</div>
							</div>	
							<div class="col-xs-9 col-sm-10">
								<div class="bubble bubble-border">
									<p class="date_coms">04/11/2015 à 21h42</p>
									<hr class="separateur"/>
									<p>
										Est-ce que quelqu'un sait si les conférences proposées seront mises en lignes après l'événement ?
									</p>
								</div>
							</div>						
						</div>

						<div class="row right_chat">
							<div class="col-xs-3 col-sm-2 badge_user">
								<div class="media">
									<a class="pull-left" href="profil.php">
										<img class="media-object dp img-circle" src="http://lorempixel.com/400/400/cats">
										<span>El Catouccino</span>
									</a>									
								</div>
							</div>
							<div class="col-xs-9 col-sm-10">
								<div class="bubble bubble-border">
									<p class="date_coms">09/02/2015 à 12:30</p>
									<hr class="separateur"/>
									<p>
										C'est en effet prévu, il n'y a cependant pas de date indiquée.
									</p>	
								</div>


							</div>
						</div>
						<div class="row left_chat">							
							<div class="col-xs-3 col-sm-2 badge_user">
								<div class="media">
									<a class="pull-right" href="#">
										<img class="media-object dp img-circle" src="http://lorempixel.com/400/400/people">
										<span>Alex</span>
									</a>									
								</div>
							</div>	
							<div class="col-xs-9 col-sm-10">
								<div class="bubble bubble-border">
									<p class="date_coms">09/02/2015 à 12:30</p>
									<hr class="separateur"/>
									<p>Merci beaucoup !</p>	
								</div>
							</div>						
						</div>
						<div class="commenter">
							<form action="indisponible.php" method="post">
								<textarea placeholder="Ajouter un commentaire" class="col-xs-12 form-control" title="nouveau_commentaire" required="required"></textarea>
								<button type="submit" class="btn btn-primary" title="commenter">Commenter</button>
							</form>
						</div>
					</div>

				</div>
				<div class="row no_margin rates">
					<div class="col-xs-12 container_rates">
						<?php include "include/rates_system.php" ?>
					</div>

				</div>
			</div>

		</div>
	</div> 
	<?php include 'include/footer.php' ?>

	</html>