<?php include 'include/header.php' ?>
<div class="container une_conf">
	<?php include 'include/breadcrumb.php' ?>

	<div class="row titre_conf hidden-xs">             
		<h1>Web2Day 2016</h1>
		<hr/>     
		<h2>Nantes du 15 au 17 juin 2016</h2>
	</div>

	<div class="row desc_conf">
		<div class="col-xs-6 col-sm-4 img_conf">
			<img class="img-responsive" src="assets/img/w2dcarte.jpg" alt="">
			<div class="no_padding hidden-xs rating">
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span>(152)</span>
				<span class="glyphicon glyphicon-heart"></span>
			</div>
		</div>
		<div class="col-xs-6 visible-xs">
			<div class="col-xs-12 titre_conf">             
				<h1>Web2Day 2016</h1>
				<hr/>     
				<h2>Nantes du 15 au 17 juin 2016</h2>
			</div>
			<div class="col-xs-12 no_padding rating">
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star-empty	"></span>
				<span>(52)</span>
				<span class="glyphicon glyphicon-heart"></span>
			</div>
			<div class="col-xs-12 logo_sociaux">
				<a href="http://www.facebook.com/share.php?u=<?php echo $url_full; ?>" class="fa fa-facebook fa-2x" title="Facebook"></a>
				<a href="https://twitter.com/share" class="fa fa-twitter fa-2x"  data-via="CodenConf" data-hashtags="ConféFrance" title="Twitter" ></a>
				<a href="https://plus.google.com/share?url=<?php echo $url_full; ?>" title="IUT de Bordeaux" ><img class="img_iut" src="assets/img/button-gplus.png" alt="logo partage google plus"/></a>
			</div>
		</div>

		<div class="col-xs-12 col-sm-8 container_desc">
			<div class="row no_margin onglets">
				<ul>
					<li class="col-xs-4 col-sm-3 active" id="description">Description</li>
					<li class="col-xs-4 col-sm-3" id="informations">Informations</li>
					<li class="col-xs-2 hidden-xs hidden-sm">
						<div class="fb-share-button" data-href="<?php echo $url;?>" data-layout="button_count"></div>
					</li>
					<li class="col-xs-2 hidden-xs hidden-sm">
						<a href="https://twitter.com/share" class="twitter-share-button" data-via="CodenConf" data-hashtags="ConféFrance">Tweet</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
					</li>
					<li class="col-xs-2 hidden-xs hidden-sm"><g:plus action="share"  annotation="bubble"></g:plus></li>
				</ul>
			</div>
			<div class="row no_margin contenu_desc description">
				<p>Le Web2day, événement web incontournable en France, propose 3 jours de conférences, de
					débats et d'animations ludiques et technophiles autour des nouveaux usages du web et des
					innovations numériques.
				</p>
				<p>Plus d'informations sur le site : <a href="http://web2day.co" target="_blank" title="site de web2day">web2day.co</a></p>
			</div>
			<div class="row no_margin contenu_desc informations">
				<p><b>	Adresse : </b></p>
				<p> Stereolux, 4 Boulevard Léon Bureau, 44200 Nantes</p>
				<p><b> 1 journée</b> : </p>
				<p> 99€</p>
				<p><b> Pass 3 jours </b> : </p>
				<p> 249€</p>
			</div>
		</div>
	</div>

	<div class="row rates_coms">
		<div class="col-xs-12">
			<div class="row no_margin onglets">
				<ul>
					<li class="col-xs-4 col-sm-3 col-md-2" id="coms">Commentaires</li>
					<li class="col-xs-4 col-sm-3 col-md-2" id="rates">Détail des notes</li>
				</ul>
			</div>
			<div class="row no_margin coms">
				<div class="col-xs-12 container_coms">
					<p>Aucun commentaire.</p>
					<div class="commenter">
						<form action="indisponible.php" method="post">
							<textarea placeholder="Ajouter un commentaire" class="col-xs-12 form-control" title="nouveau_commentaire" required="required"></textarea>
							<button type="submit" class="btn btn-primary" title="commenter">Commenter</button>
						</form>
					</div>
				</div>
			</div>

			<div class="row no_margin rates">
				<div class="col-xs-12 container_rates">
					<?php include "include/rates_system.php" ?>
				</div>
			</div>

		</div>
	</div>
</div> 
<?php include 'include/footer.php' ?>

</html>
